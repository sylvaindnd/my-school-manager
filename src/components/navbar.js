/* eslint-disable no-tabs */
export default class NavBar {
  constructor() {
    this.timer = undefined;
  }

  render() {
    const nav = `
        <nav class="navbar">
				<div class="container-fluid">
					<div class="nav_container">
						<ul class="navbar-nav d-flex">
							<li class="nav-item">
								<a class="nav-link" href="/student">My Profile</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/classes">Classes</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="/students">Students</a>
							</li>	
						</ul>
						<form id="nav__search" class="d-flex">
							<input id="nav_search__input" class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
							<button class="btn btn-primary" type="submit">Search</button>
						</form>
					</div>
				</div>
			</nav>`;
    this.waitDom();
    return nav;
  }

  waitDom() {
    if (!this.timer) {
      this.timer = setInterval(() => {
        if (document.getElementById('nav__search')) {
          clearInterval(this.timer);
          this.timer = undefined;
          this.events();
        }
      }, 100);
    }
  }

  events() {
    document.getElementById('nav__search').addEventListener('submit', (event) => {
      event.preventDefault();
      this.sendSearch(event.target);
    });
  }

  sendSearch(form) {
    const axios = require('axios');
    const search = form.querySelector('#nav_search__input').value;

    const config = {
      method: 'get',
      url: `https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/search?search=${search}`,
      headers: {}
    };

    axios(config)
      .then((response) => {
        localStorage.setItem('searchInfos', JSON.stringify(response.data));
        window.location.href = '/search';
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }
}
