/* eslint-disable no-tabs */
const axios = require('axios');

export default class SigninPage {
  renderPage() {
    const page = `
	<div id="signin_form" class="signin_form d-flex flex-column">
		<div class="signin_title">
			<h1>Sign In</h1>
		</div>	
		<form id="signin_form__form">
			<div class="row">
				<label for="signin_input__email">Email</label>
				<input id="signin_input__email" type="text" class="form-control" placeholder="Email">
			</div>
			<div class="row">
				<label for="signin_input__password">Password</label>
				<input id="signin_input__password" type="password" class="form-control" placeholder="Password">
			</div>				

      <div class="row">
				<span class="input__error"></span>									
			</div>

			<div class="row">
				<button id="signin_submit" ype="submit" class="btn btn-primary">Signin</button>									
			</div>				

			<div class="row">
				<span>You don't have an account yet : <a href="/signup">signup</a></span>	
			</div>
		</form>
	</div>`;

    return page;
  }

  events() {
    document.getElementById('signin_form__form').addEventListener('submit', (event) => {
      event.preventDefault();
      this.sendForm(event.target);
    });
  }

  sendForm(form) {
    const email = form.querySelector('#signin_input__email').value;
    const password = form.querySelector('#signin_input__password').value;

    // eslint-disable-next-line max-len
    if (!email.length || !password.length) {
      // eslint-disable-next-line no-param-reassign
      form.querySelector('.input__error').innerHTML = 'You must fill in all the fields.';
      return;
    }
    const data = JSON.stringify({
      email,
      password
    });

    const config = {
      method: 'post',
      url: 'https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/signin',
      headers: {
        'x-api-key': 'I4fyCTUia3gPsIqBmMZh6Jl1IIzd9it5GQs07WOa',
        'Content-Type': 'application/json'
      },
      data
    };

    axios(config)
      .then((response) => {
        if (response.data.status !== 'ok') {
          // eslint-disable-next-line no-param-reassign
          form.querySelector('.input__error').innerHTML = response.data.message;
        } else {
          const student = response.data.account;
          localStorage.setItem('studentInfos', JSON.stringify(student));
          window.location.href = '/student';
        }
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }
}
