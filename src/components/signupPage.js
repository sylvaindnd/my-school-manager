/* eslint-disable no-nested-ternary */
/* eslint-disable no-useless-return */
/* eslint-disable no-tabs */
const axios = require('axios');

export default class SignupPage {
  renderPage() {
    const page = `
        <div id="signup_form" class="signup_form d-flex flex-column">
			<div class="signup_title">
				<h1>Sign Up</h1>
			</div>	
			<form id="signup_form__form">
				<div class="row">
					<div class="col">
						<label for="signup_input__firstname">First name</label>
						<input id="signup_input__firstname" type="text" class="form-control" placeholder="First name">
					</div>
					<div class="col">
						<label for="signup_input__lastname">Last name</label>
						<input id="signup_input__lastname" type="text" class="form-control" placeholder="Last name">
					</div>
				</div>
				<div class="row">
					<label for="signup_input__email">Email</label>
					<input id="signup_input__email" type="text" class="form-control" placeholder="Email">
				</div>
				<div class="row">
					<div class="col">
						<label for="signup_input__birthday">Birthday</label>
						<input id="signup_input__birthday" type="date" class="form-control" placeholder="Birthday">
					</div>
					<div class="col">
						<label for="signup_input__gender">Gender</label>
						<select id="signup_input__gender" class="form-control">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<label for="signup_input__promotion">Promotion</label>
						<select id="signup_input__promotion" class="form-control">
						</select>
					</div>
					<div class="col">
						<label for="signup_input__speciality">Speciality</label>
						<select id="signup_input__speciality" class="form-control">
							<option value="Developer">Developer</option>
              <option value="Designer">Designer</option>
							<option value="Marketing">Marketing</option>
						</select>
					</div>
				</div>
				<div class="row">
					<label for="signup_input__password">Password</label>
					<input id="signup_input__password" type="password" class="form-control" placeholder="Password">
				</div>
				<div class="row">
					<label for="signup_input__password2">Confirm password</label>
					<input id="signup_input__password2" type="password" class="form-control" placeholder="Confirm password">
				</div>

        <div class="row">
					<span class="input__error"></span>									
				</div>
				
				<div class="row">
					<button id="signup_submit" type="submit" class="btn btn-primary">Signup</button>									
				</div>				

				<div class="row">
					<span>You already have an account : <a href="/signin">signin</a></span>	
				</div>
			</form>
		</div>`;

    return page;
  }

  events() {
    this.addClasses();
    document.getElementById('signup_form__form').addEventListener('submit', (event) => {
      event.preventDefault();
      this.sendForm(event.target);
    });
  }

  generateId(length = 16) {
    const chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let result = '';
    // eslint-disable-next-line no-plusplus
    for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

  sendForm(form) {
    const firstname = form.querySelector('#signup_input__firstname').value;
    const lastname = form.querySelector('#signup_input__lastname').value;
    const email = form.querySelector('#signup_input__email').value;
    const birthday = form.querySelector('#signup_input__birthday').value;
    const gender = form.querySelector('#signup_input__gender').value;
    const promotion = form.querySelector('#signup_input__promotion').value;
    const speciality = form.querySelector('#signup_input__speciality').value;
    const password = form.querySelector('#signup_input__password').value;
    const password2 = form.querySelector('#signup_input__password2').value;
    const uuid = this.generateId(16);

    // eslint-disable-next-line max-len
    if (!firstname.length || !lastname.length || !email.length || !birthday.length || !gender.length || !promotion.length || !speciality.length || !password.length) {
      // eslint-disable-next-line no-param-reassign
      form.querySelector('.input__error').innerHTML = 'You must fill in all the fields.';
      return;
    }

    if (password !== password2) {
      // eslint-disable-next-line no-param-reassign
      form.querySelector('.input__error').innerHTML = 'Passwords do not match.';
      return;
    }

    const data = JSON.stringify({
      id: uuid,
      firstname,
      lastname,
      email,
      birthday,
      gender,
      id_promotion: promotion,
      id_speciality: speciality,
      password
    });

    const config = {
      method: 'post',
      url: 'https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/signup',
      headers: {
        'x-api-key': 'I4fyCTUia3gPsIqBmMZh6Jl1IIzd9it5GQs07WOa',
        'Content-Type': 'application/json'
      },
      data
    };

    axios(config)
      .then((response) => {
        if (response.data.status !== 'ok') {
          // eslint-disable-next-line no-param-reassign
          form.querySelector('.input__error').innerHTML = response.data.message;
        } else {
          const student = response.data.account;
          localStorage.setItem('studentInfos', JSON.stringify(student));
          window.location.href = '/student';
        }
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }

  addClasses() {
    const config = {
      method: 'get',
      url: 'https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/classes',
      headers: {}
    };

    axios(config)
      .then((response) => {
        if (response.data.status === 'ok') {
          const classesArray = response.data.classes;
          // eslint-disable-next-line radix
          classesArray.sort((a, b) => ((a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0)));
          let list = '';
          classesArray.forEach((classe) => {
            list += `<option value="${classe.name}">${classe.name}</option>`;
          });
          document.getElementById('signup_form').querySelector('#signup_input__promotion').innerHTML = list;
        }
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }
}
