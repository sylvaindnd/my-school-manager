export default class Routing {
  constructor(defaultName, defaultPage, pages) {
    this.defaultName = defaultName;
    this.defaultPage = defaultPage;
    this.pages = pages;
    this.redirectPages();
  }

  changeUrlByPages(name) {
    window.history.pushState(name, name, `${window.location.origin}/${name}`);
  }

  getCurrentUrlParameters(queryString = false) {
    const urlParameters = window.location.href.replace(window.location.origin, '').split('/').slice(1);
    if (queryString) {
      return urlParameters;
    }
    return Array.from(urlParameters, (x) => x.split('?')[0]);
  }

  addUrlParametersLocalStorage() {
    localStorage.setItem('urlParameters', this.getCurrentUrlParameters(true)[0]);
  }

  redirectPages() {
    const urlParameters = this.getCurrentUrlParameters(false);
    let name = this.defaultName;
    let page = this.defaultPage;
    // eslint-disable-next-line no-restricted-syntax
    for (const [pageName, Page] of Object.entries(this.pages)) {
      // eslint-disable-next-line no-loop-func
      urlParameters.forEach((urlParameter) => {
        if (urlParameter === pageName) {
          page = Page;
          name = pageName;
        }
      });
    }

    this.addUrlParametersLocalStorage();
    this.changeUrlByPages(name);
    document.getElementById('app').innerHTML = page.renderPage();
    page.events();
    return page;
  }
}
