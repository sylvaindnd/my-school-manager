/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-tabs */
// eslint-disable-next-line no-unused-vars
import NavBar from './navbar';

const axios = require('axios');

export default class SearchPage {
  checkStudentInfos() {
    const studentInfos = localStorage.getItem('studentInfos');
    if (studentInfos) {
      this.infos = JSON.parse(studentInfos);
      return true;
    }
    window.location.href = '/signin';
    return false;
  }

  checkSearchInfos() {
    const searchInfos = localStorage.getItem('searchInfos');
    if (searchInfos) {
      this.search = JSON.parse(searchInfos);
      return true;
    }
    window.location.href = '/student';
    return false;
  }

  renderPage() {
    this.checkStudentInfos();
    this.checkSearchInfos();
    const nav = new NavBar();
    const page = `
    <div class="app_container">
      ${nav.render()}
			<div id="classe_students_list">
				<div class="classes_title">
					<h1>Search: <b></b></h1>
					<h2><b></b> students</h2>
				</div>
				<div class="students_list-group">						

				</div>
				<div id="classes_back_profil">
					<span>Back to : <a id="button_disconnect" href="/classes">classes list</a></span>
				</div>
			</div>
		</div>`;

    return page;
  }

  events() {
    this.addStudents();
  }

  addStudents() {
    const studentsArray = this.search.students;
    // eslint-disable-next-line radix
    studentsArray.sort((a, b) => ((a.lastname > b.lastname) ? 1 : ((b.lastname > a.lastname) ? -1 : 0)));
    let list = '';
    studentsArray.forEach((student) => {
      list += `<div class="card">
						<img src="${student.image ? student.image : 'https://via.placeholder.com/150'}" class="card-img-top" alt="picture">
						<div class="card-body" id="student__student-${student.id}">
							<h5 class="card-title">${student.lastname} ${student.firstname}</h5>
							<h6 class="card-title">${student.id_speciality}</h6>
							<div class="row">
								<div class="col"><a href="" class="btn btn-primary edit">Edit</a></div>
								<div class="col"><a href="" class="btn btn-danger delete">Delete</a></div>
							</div>
						</div>
					</div>`;
    });
    document.getElementById('classe_students_list').querySelector('.students_list-group').innerHTML = list;
    document.getElementById('classe_students_list').querySelector('.classes_title h1 b').innerHTML = this.search.search;
    document.getElementById('classe_students_list').querySelector('.classes_title h2 b').innerHTML = studentsArray.length;
    this.eventsStudentButton(studentsArray);
  }

  eventsStudentButton(students) {
    students.forEach((student) => {
      document.getElementById(`student__student-${student.id}`).querySelector('a.edit').addEventListener('click', (event) => {
        event.preventDefault();
        this.redirectToProfil(student.id);
      });
    });
  }

  redirectToProfil(studentid) {
    const config = {
      method: 'get',
      url: `https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/?id=${studentid}`,
      headers: {}
    };

    axios(config)
      .then((response) => {
        const studentsArray = response.data;
        localStorage.setItem('profileInfos', JSON.stringify(studentsArray));
        window.location.href = '/profile';
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }
}
