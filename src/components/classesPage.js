/* eslint-disable no-nested-ternary */
/* eslint-disable no-tabs */
// eslint-disable-next-line no-unused-vars
import NavBar from './navbar';

const axios = require('axios');

export default class ClassesPage {
  checkStudentInfos() {
    const studentInfos = localStorage.getItem('studentInfos');
    if (studentInfos) {
      this.infos = JSON.parse(studentInfos);
      return true;
    }
    window.location.href = '/signin';
    return false;
  }

  renderPage() {
    this.checkStudentInfos();
    const nav = new NavBar();
    const page = `
    <div class="app_container">	
    ${nav.render()}
			<div id="classes_list">
				<div class="classes_title">
					<h1>Classes list (<b></b>)</h1>
				</div>
				<ul class="classes_list-group list-group">
				</ul>
				<div id="classes_back_profil">
					<span>Back to : <a id="button_disconnect" href="/student">my profile</a></span>
				</div>
			</div>			
		</div>`;

    return page;
  }

  events() {
    this.addClasses();
  }

  addClasses() {
    const self = this;
    const config = {
      method: 'get',
      url: 'https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/classes',
      headers: {}
    };

    axios(config)
      .then((response) => {
        if (response.data.status === 'ok') {
          const classesArray = response.data.classes;
          // eslint-disable-next-line radix
          classesArray.sort((a, b) => ((a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0)));
          let list = '';
          classesArray.forEach((classe) => {
            list += `<a id="classes_classe__classe-${classe.id}" href="" data-classe="${classe.id}"><li class="list-group-item">${classe.name}</li></a>`;
          });
          document.getElementById('classes_list').querySelector('.classes_list-group').innerHTML = list;
          document.getElementById('classes_list').querySelector('.classes_title h1 b').innerHTML = classesArray.length + 0;
          self.eventsClassButton(classesArray);
        }
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }

  eventsClassButton(classes) {
    classes.forEach((classe) => {
      document.getElementById(`classes_classe__classe-${classe.id}`).addEventListener('click', (event) => {
        event.preventDefault();
        this.redirectToClasses(classe.id, classe.name);
      });
    });
  }

  redirectToClasses(classeid, classename) {
    localStorage.setItem('classeid', classeid);
    localStorage.setItem('classename', classename);
    window.location.href = '/classe';
  }
}
