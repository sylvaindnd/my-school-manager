/* eslint-disable no-nested-ternary */
/* eslint-disable no-tabs */
// eslint-disable-next-line no-unused-vars
import NavBar from './navbar';

// eslint-disable-next-line no-unused-vars
const axios = require('axios');

export default class StudentPage {
  checkStudentInfos() {
    const studentInfos = localStorage.getItem('profileInfos');
    if (studentInfos) {
      this.infos = JSON.parse(studentInfos);
      return true;
    }
    window.location.href = '/signin';
    return false;
  }

  renderPage() {
    this.checkStudentInfos();
    const nav = new NavBar();
    const page = `
    <div class="app_container">		
      ${nav.render()}
      <div id="student_form" class="student_form d-flex flex-column">
        <div class="student_title">
          <h1>Edit : <b>${this.infos.firstname}</b></h1>
          <h6 class="text-primary">Student profile editor page</h6>
        </div>
        <form id="student_form__form" data-student="${this.infos.id}">
          <div class="row">
            <div class="col">
              <img id="student_input__picture-image" src="${this.infos.image ? this.infos.image : 'https://via.placeholder.com/150'}" class="student_input__image img-thumbnail"
                alt="picture">
            </div>

            <div class="col">
              <label for="student_input__picture">Picture Url</label>
              <input id="student_input__picture" type="text" class="form-control"
                placeholder="https://via.placeholder.com/150" value="${this.infos.image ? this.infos.image : ''}">
            </div>
          </div>
          <div class="row">
            <div class="col">
              <label for="student_input__firstname">First name</label>
              <input id="student_input__firstname" type="text" class="form-control"
                placeholder="First name" value="${this.infos.firstname}">
            </div>
            <div class="col">
              <label for="student_input__lastname">Last name</label>
              <input id="student_input__lastname" type="text" class="form-control"
                placeholder="Last name" value="${this.infos.lastname}">
            </div>
          </div>
          <div class="row">
            <label for="student_input__email">Email</label>
            <input id="student_input__email" type="text" class="form-control" placeholder="Email" value="${this.infos.email}">
          </div>
          <div class="row">
            <div class="col">
              <label for="student_input__birthday">Birthday</label>
              <input id="student_input__birthday" type="date" class="form-control" placeholder="Birthday" value="${this.infos.birthday}">
            </div>
            <div class="col">
              <label for="student_input__gender">Gender : <b>${this.infos.gender}</b></label>
              <select id="student_input__gender" class="form-control" data-item="${this.infos.gender}">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <label for="student_input__promotion">Promotion : <b>${this.infos.id_promotion}</b></label>
              <select id="student_input__promotion" class="form-control" data-item="${this.infos.id_promotion}">
              </select>
            </div>
            <div class="col">
              <label for="student_input__speciality">Speciality : <b>${this.infos.id_speciality}</b></label>
              <select id="student_input__speciality" class="form-control" data-item="${this.infos.id_speciality}">
                <option value="Developer">Developer</option>
                <option value="Designer">Designer</option>
                <option value="Marketing">Marketing</option>
              </select>
            </div>
          </div>
          <div class="row">
            <label for="student_input__password">Password</label>
            <input id="student_input__password" type="password" class="form-control" placeholder="Password">
          </div>
          <div class="row">
            <label for="student_input__password2">Confirm password</label>
            <input id="student_input__password2" type="password" class="form-control"
              placeholder="Confirm password">
          </div>

          <div class="row">
            <span class="input__error"></span>
          </div>

          <div class="row">
            <button id="student_submit" type="submit" class="btn btn-primary">Update information</button>
          </div>

          <div class="row">
            <span>Back to : <a id="button_disconnect" href="/classes">classes list</a></span>
          </div>
        </form>
      </div>
    </div>`;

    return page;
  }

  events() {
    this.addClasses();

    document.getElementById('student_form__form').addEventListener('submit', (event) => {
      event.preventDefault();
      this.sendForm(event.target);
    });

    document.getElementById('button_disconnect').addEventListener('click', (event) => {
      event.preventDefault();
      window.location.href = '/classes';
    });

    document.getElementById('student_input__picture').addEventListener('input', (event) => {
      let valueImage = event.target.value;
      if (!valueImage) {
        valueImage = 'https://via.placeholder.com/150';
      }
      document.getElementById('student_input__picture-image').setAttribute('src', valueImage);
    });
  }

  // eslint-disable-next-line no-unused-vars
  sendForm(form) {
    const image = form.querySelector('#student_input__picture').value;
    const firstname = form.querySelector('#student_input__firstname').value;
    const lastname = form.querySelector('#student_input__lastname').value;
    const email = form.querySelector('#student_input__email').value;
    const birthday = form.querySelector('#student_input__birthday').value;
    const gender = form.querySelector('#student_input__gender').value;
    const promotion = form.querySelector('#student_input__promotion').value;
    const speciality = form.querySelector('#student_input__speciality').value;
    const password = form.querySelector('#student_input__password').value;
    const password2 = form.querySelector('#student_input__password2').value;
    const uuid = form.getAttribute('data-student');

    // eslint-disable-next-line max-len
    if (!firstname.length || !lastname.length || !email.length || !birthday.length || !gender.length || !promotion.length || !speciality.length) {
      // eslint-disable-next-line no-param-reassign
      form.querySelector('.input__error').innerHTML = 'You must fill in all the fields (leave the password empty if you have not changed it).';
      return;
    }

    if (password !== password2) {
      // eslint-disable-next-line no-param-reassign
      form.querySelector('.input__error').innerHTML = 'Passwords do not match.';
      return;
    }

    const data = JSON.stringify({
      id: uuid,
      image,
      firstname,
      lastname,
      email,
      birthday,
      gender,
      id_promotion: promotion,
      id_speciality: speciality,
      password
    });

    const config = {
      method: 'post',
      url: 'https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/update',
      headers: {
        'x-api-key': 'I4fyCTUia3gPsIqBmMZh6Jl1IIzd9it5GQs07WOa',
        'Content-Type': 'application/json'
      },
      data
    };

    axios(config)
      .then((response) => {
        if (response.data.status !== 'ok') {
          // eslint-disable-next-line no-param-reassign
          form.querySelector('.input__error').innerHTML = response.data.message;
        } else {
          const student = response.data.account;
          localStorage.setItem('profileInfos', JSON.stringify(student));
          window.location.href = '/profile';
        }
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }

  addClasses() {
    const self = this;
    const config = {
      method: 'get',
      url: 'https://rwtiw74l71.execute-api.eu-west-3.amazonaws.com/my-digital-school_stage/classes',
      headers: {}
    };

    axios(config)
      .then((response) => {
        if (response.data.status === 'ok') {
          const classesArray = response.data.classes;
          // eslint-disable-next-line radix
          classesArray.sort((a, b) => ((a.id > b.id) ? 1 : ((b.id > a.id) ? -1 : 0)));
          let list = '';
          classesArray.forEach((classe) => {
            list += `<option value="${classe.name}">${classe.name}</option>`;
          });
          document.getElementById('student_form').querySelector('#student_input__promotion').innerHTML = list;
          self.studentSelects();
        }
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.log(error);
      });
  }

  studentSelects() {
    const gender = document.getElementById('student_input__gender');
    const promotion = document.getElementById('student_input__promotion');
    const speciality = document.getElementById('student_input__speciality');

    promotion.querySelectorAll('option').forEach((item) => {
      if (item.value === promotion.getAttribute('data-item')) {
        item.setAttribute('selected', true);
      }
    });

    gender.querySelectorAll('option').forEach((item) => {
      if (item.value === gender.getAttribute('data-item')) {
        item.setAttribute('selected', true);
      }
    });

    speciality.querySelectorAll('option').forEach((item) => {
      if (item.value === speciality.getAttribute('data-item')) {
        item.setAttribute('selected', true);
      }
    });
  }
}
