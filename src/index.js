/* eslint-disable no-unused-vars */
import './index.scss';
import SignupPage from './components/signupPage';
import SigninPage from './components/signinPage';
import StudentPage from './components/studentPage';
import ClassesPage from './components/classesPage';
import ClassePage from './components/classePage';
import ProfilePage from './components/profilePage';
import StudentsPage from './components/studentsPage';
import SearchPage from './components/searchPage';
import Routing from './components/routing';

const signupPage = new SignupPage();
const signinPage = new SigninPage();
const studentPage = new StudentPage();
const classesPage = new ClassesPage();
const classePage = new ClassePage();
const profilePage = new ProfilePage();
const studentsPage = new StudentsPage();
const searchPage = new SearchPage();

const pages = {
  signin: signinPage,
  signup: signupPage,
  student: studentPage,
  classes: classesPage,
  classe: classePage,
  profile: profilePage,
  students: studentsPage,
  search: searchPage
};

const rout = new Routing('signup', signupPage, pages);
