# My School Manager

## Basic Overview
My school manager is an efficient tool for creating student accounts. It allows you to manage multiple classes, specialties and students. With this tool school management has never been so easy.

## Installation

- Transfer files

    > git clone

    Transfer the files to a new folder with the command `git clone https://gitlab.com/sylvaindnd/my-school-manager.git`

- Install packages

    > npm install 

    Install all the necessary packages with the command `npm install`

- Run project 

    > npm start

    Launch the project with the command `npm start`. After, go to `localhost:9090`

## How to use
- SignUp

    > `/signup`
        
    The registration page allows you to add a student to the school. The information requested is: his **first name**, **last name**, his **date of birth**, an **email**, **gender**, a **promotion**, a **speciality**, and a **password**

- SignIn

    > `/signin`

    The login page provides access to a student's data. To log in you need to use the student's **email address** and **password**.

- Edit Profil

    > `/student`

    Editing a student profile allows you to update the following information: **first name**, **last name**, **date of birth**, **email**, **gender**, **promotion** But also to add a profile picture

- Classes

    > `/classes`

    This page allows you to see all the classes in the school and to access the list of students in each class.

- Search

    > `/search?=[infos]`

    The search allows you to find a student from any of their information.

## Environment 

This application was developed using **AWS** services. The database is in Dynamodb and is easily editable. New features can be added easily.
 
The service runs on a **WebPack** and **Eslint** environment.

## API Requests

- SignIn :

    ```
    method : POST,
    url : [url:API]/signin,

    headers : {
        x-api-key: [str:KEY],
        Content-Type: 'application/json'
    },

    data : {
        [str: email], 
        [str: password]
    }
    ```

    > Signin an user.

- SignUp :

    ```
    method : POST,
    url : [url:API]/signup,

    headers : {
        x-api-key: [str:KEY],
        Content-Type: 'application/json'
    },

    data : {
        [str: randomID],
        [str: firstname],
        [str: lastname], 
        [str: email],
        [date: birthday],
        [int: genderID],
        [int: promotionID],
        [int: specialityID],
        [str: password]
    }
    ```

    > Register an user.

- Update profile:
    ```
    method : POST,
    url : [url:API]/update,

    headers : {
        x-api-key: [str:KEY],
        Content-Type: 'application/json'
    },

    data : {
        [str: randomID],
        [str: firstname],
        [str: lastname], 
        [str: email],
        [date: birthday],
        [int: genderID],
        [int: promotionID],
        [int: specialityID],
        [str: password]
    }
    ```

    > Update an user.

- Get Classes :
    ```
    method : GET,
    url : [url:API]/classes,

    headers : {
        NULL
    },

    data : {
        NULL
    }
    ```

    > Return all classes list.

- Get Classe :

    ```
    method : GET,
    url : [url:API]/classe?id=[str: classNAME],

    headers : {
        NULL
    },

    data : {
        NULL
    }
    ```

    > Return one class by name.

- Search :

    ```
    method : GET,
    url : [url:API]/search?search=[str: search],

    headers : {
        NULL
    },

    data : {
        NULL
    }
    ```

    > Return list of user by string

- GET user :

    ```
    method : GET,
    url : [url:API]/?id=[int: userID],

    headers : {
        NULL
    },

    data : {
        NULL
    }
    ```

    > Get user by ID

- DELETE user :

    ```
    method : DELETE,
    url : [url:API]/?id=[int: userID],

    headers : {
        NULL
    },

    data : {
        NULL
    }
    ```

    > Delete user by ID

- POST user :

    ```
    method : POST,
    url : [url:API]/,

    headers : {
        x-api-key: [str:KEY],
        Content-Type: 'application/json'
    },

    data : [object: userINFOS]
    ```

    > POST user with informations object

- PUT user :

    ```
    method : PUT,
    url : [url:API]/,

    headers : {
        x-api-key: [str:KEY],
        Content-Type: 'application/json'
    },

    data : [object: userINFOS]
    ```

    > PUT user with informations object